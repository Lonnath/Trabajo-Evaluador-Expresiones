/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import ufps.util.colecciones_seed.*;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato; //+ "<->";
        }
        return msg;
    }

    public String getPrefijo() {
        Pila<String> datos = new Pila();
        Pila<String> respaldo = new Pila();
        Pila<String> operadores = new Pila();
        String palabra = "";
        for (String var : this.expresiones) {
            datos.apilar(var);
        }

        while (!datos.esVacia()) {
            String datoTemporal = datos.desapilar();

            if (precedencia(datoTemporal) == 0) {
                try {
                    Float.parseFloat(datoTemporal);
                    respaldo.apilar(datoTemporal);
                } catch (NumberFormatException e) {
                    System.err.println(e.getMessage());
                }
            } else if (precedencia(datoTemporal) == 5) {
                if (!operadores.esVacia()) {
                    String temporal = operadores.desapilar();

                    while (!temporal.equals(")")) {
                        respaldo.apilar(temporal);
                        if (!operadores.esVacia()) {
                            temporal = operadores.desapilar();
                        }
                    }
                }
            } else {
                if (!operadores.esVacia()) {
                    if (precedencia(datoTemporal) <= precedencia(operadores.getTope()) && precedencia(operadores.getTope()) < 6) {
                        respaldo.apilar(operadores.desapilar());
                    }
                }
                operadores.apilar(datoTemporal);
            }
        }

        while (!operadores.esVacia()) {
            respaldo.apilar(operadores.desapilar());
        }

        while (!respaldo.esVacia()) {
            palabra += respaldo.desapilar() + ";";
        }
        return palabra;
    }

    public String getPosfijo() {
        Pila<String> operadores = new Pila();
        String palabra = "";
        for (String dato : this.expresiones) {
            switch (precedencia(dato)) {
                case 0: {
                    try {
                        Float.parseFloat(dato);
                        palabra += ";" + dato;
                    } catch (NumberFormatException e) {
                        System.err.println(e.getMessage());
                    }
                    break;

                }
                case 6: {
                    String constantinopla = operadores.desapilar();
                    while (!constantinopla.equals("(")) {
                        palabra += ";" + constantinopla;
                        constantinopla = operadores.desapilar();
                    }
                }

                default: {
                    if (!operadores.esVacia()) {
                        if (precedencia(dato) <= precedencia(operadores.getTope()) && !operadores.getTope().equals("(") && !dato.equals(")")) {
                            palabra += ";" + operadores.desapilar();
                            operadores.apilar(dato);
                        } else {
                            if (!dato.equals(")")) {
                                operadores.apilar(dato);
                            }
                        }
                    } else {
                        operadores.apilar(dato);
                    }
                }

            }

        }
        while (!operadores.esVacia()) {
            String constantinopla = operadores.desapilar();

            if (!constantinopla.equals("(")) {
                palabra += ";" + constantinopla;
            }
        }
        return palabra;
    }

    private int precedencia(String a) {
        switch (a) {
            case "+":
                return 1;

            case "-":
                return 2;

            case "*":
                return 3;

            case "/":
                return 4;

            case "(":
                return 5;

            case ")":
                return 6;

            default:
                return 0;

        }
    }

    public float getEvaluarPosfijo() {
        Pila<Float> numeros = new Pila();
        String v[] = this.getPosfijo().split(";");
      
        for (String dato:v) {
            switch (dato) {
                case "+": {
                    numeros.apilar(numeros.desapilar() + numeros.desapilar());
                     break;
                }
                case "-": {
                     numeros.apilar(numeros.desapilar() - numeros.desapilar());
                   break;
                }
                case "*": {
                     numeros.apilar(numeros.desapilar()* numeros.desapilar());
                     break;
                }
                case "/": {
                    float var = numeros.desapilar() ;
                    float var1 = numeros.desapilar();
                    numeros.apilar(var1/var);
                   break;
                }

                default: {
                    try {
                       
                        numeros.apilar(Float.parseFloat(dato));
                    } catch (NumberFormatException e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                }
            }
        }
        return numeros.desapilar();
    }
}